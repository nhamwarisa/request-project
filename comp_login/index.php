<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="author" content="Sahil Kumar">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Complete User Registration & Login</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
    	#alert,#register-box,#forgot-box,#loader{
    	display:none;}
    </style>

</head>
<body background="pic/m01.jpg" >
	<div class="container mt-4">
		<!-- Login Form -->
		<div class="row">
			<div class="col-lg-4 offset-lg-4" id="alert">
				<div class="alert alert-success">
					<strong id="result"></strong>
				</div>
				
			</div>
		</div>
		<div class="text-center">
			<img src="pic/loadc.gif" width="60px" height="60px" class="m-2" id="loader">
		</div>
		<div class="row">
			<div class="col-lg-4 offset-lg-4 bg-light rounded" id="login-box">
				<h2 class="text-center mt-2">Login</h2>
				<form name="flogin" action="session.php" method="post" role="form" class="p-2" id="login-form">
					<div class="form-group">
						<input type="email" name="email"  class="form-control" placeholder="Enter your email" required value="<?php if(isset($_COOKIE['email'])){ echo $_COOKIE['email'];} ?>">
					</div>
					<div class="form-group">
						<input type="password" name="password"  class="form-control" placeholder="Enter your password" required minlength="8" value="<?php if(isset($_COOKIE['password'])){ echo $_COOKIE['password'];} ?>">
					</div>
					<div class="form-group">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" name="rem" class="custom-control-input" id="customCheck" <?php if(isset($_COOKIE['email'])) {?> checked <?php  }?>>
							<label for="customCheck" class="custom-control-label">Remember Me</label>
							<a href="#" id="forgot-btn" class="float-right">Forgot Password?</a>
						</div>
					</div>
					<div class="form-group">
						<input type="submit" name="login" id="login" value="login" class="btn btn-primary btn-block">
					</div>
					<div class="form-group">
						<p class="text-center">New User? <a href="#" id="register-btn">Register Here</a></p>
					</div>
					
				</form>
			</div>
		</div>



        <!-- Registation Form -->
		<div class="row">
			<div class="col-lg-4 offset-lg-4 bg-light rounded" id="register-box">
				<h2 class="text-center mt-2">Register</h2>
				<form action="" method="post" role="form" class="p-2" id="register-form">
					<div class="form-group">
						<input type="text" name="fname" class="form-control" placeholder="Full Name" required minlength="2">
					</div>
					<div class="form-group">
						<input type="text" name="lname" class="form-control" placeholder="Last name" required minlength="2">
					</div>
					<div class="form-group">
						<input type="email" name="remail" class="form-control" placeholder="Email" required >
					</div>
					<div class="form-group">
						<input type="password" name="pass" id="pass" class="form-control" placeholder="Password" required minlength="8">
					</div>
					<div class="form-group">
						<input type="password" name="cpass" id="cpass" class="form-control" placeholder="Confirm Password" required minlength="8">
					</div>
					<!-- <div class="form-group">
						<input type="file" name="image" id="image">
					</div> -->
					<div class="form-group">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" name="rem" class="custom-control-input" id="customCheck2">
							<label for="customCheck2" class="custom-control-label">I agree to the <a href="#">term & condition.</a></label>
							</div>
					</div>
					<div class="form-group">
						<input type="submit" name="register" id="register" value="Register" class="btn btn-primary btn-block">
					</div>
					<div class="form-group">
						<p class="text-center">Already Registered? <a href="#" id="login-btn">Login Here</a></p>
					</div>
				</form>
			</div>
		</div>


		<!-- Forgot Password -->
		<div class="row">
			<div class="col-lg-4 offset-lg-4 bg-light rounded" id="forgot-box">
				<h2 class="text-center mt-2">Reset Password</h2>
				<form action="" method="post" role="form" class="p-2" id="forgot-form">
					<div class="form-group">
						<small class="text-muted"> To reset your password,enter the email address and we will send reset password instructions on your email. 
						</small>
					</div>
					<div class="form-group">
						<input type="email" name="femail" class="form-control" placeholder="Enter your email" required>
					</div>
					
					<div class="form-group">
						<input type="submit" name="forgot" id="forgot" value="Reset" class="btn btn-primary btn-block">
					</div>
					<div class="form-group text-center">
						 <a href="#" id="back-btn" >Back</a>
					</div>
				</form>
			</div>
		</div>

	</div>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script type="text/javascript">
	$(document).ready(function(){
		$("#register-btn").click(function(){
			$("#login-box").hide();
			$("#register-box").show();

		});
        $("#login-btn").click(function(){
			$("#register-box").hide();
			$("#login-box").show();

		});
		$("#forgot-btn").click(function(){
			$("#login-box").hide();
			$("#forgot-box").show();

		});
		$("#back-btn").click(function(){
			$("#forgot-box").hide();
			$("#login-box").show();

		});
		$("#login-form").validate();
		$("#register-form").validate({
         rules:{
            cpass:{
            	equalTo:"#pass",
            }
		 }
		});
		$("#forgot-form").validate();

		// submit form without page refresh
		$("#register").click(function(e){
           if(document.getElementById('register-form').checkValidity()){
           	e.preventDefault();
           	$("#loader").show();
           	$.ajax({
           		url:'action.php',
           		method:'post',
           		data:$("#register-form").serialize()+ '&action=register',
           		success:function(response){
                    $("#alert").show();
                    $("#result").html(response);
                    $("#loader").hide();
           		}
           	});
           }
           return true;
		});


		$("#login").click(function(e){
           if(document.getElementById('login-form').checkValidity()){
           	e.preventDefault();
           	$("#loader").show();
           	$.ajax({
           		url:'action.php',
           		method:'post',
           		data:$("#login-form").serialize()+ '&action=login',
           		success:function(response){
           			if (response==="ok") {
           				window.location='session.php';
           			}
           			else{
           			$("#alert").show();
                    $("#result").html(response);
                    $("#loader").hide();
                }
                    
           		}
           	});
           }
           return true;
		});

		
	


		$("#forgot").click(function(e){
           if(document.getElementById('forgot-form').checkValidity()){
           	e.preventDefault();
           	$("#loader").show();
           	$.ajax({
           		url:'action.php',
           		method:'post',
           		data:$("#forgot-form").serialize()+ '&action=forgot',
           		success:function(response){
                    $("#alert").show();
                    $("#result").html(response);
                    $("#loader").hide();
           		}
           	});
           }
           return true;
		});
	});
</script>
</body>
</html>




