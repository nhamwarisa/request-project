<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="author" content="Sahil Kumar">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Contact </title>
	 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


</head>
<body background="pic/m01.jpg" bgproperties=fixed>
	<nav class="navbar navbar-expand-sm bg-success navbar-dark">
  <!-- Brand -->
  <!-- <a class="navbar-brand " href="#">MIS</a> -->
  <nav class="navbar navbar-light bg-faded">
  <a class="navbar-brand" href="#">
    <img src="pic/Logo_MED_TH.png" width="80" height="80" alt="">
  </a>
</nav>


<p class="navbar navbar-light ml-auto"></p>
<nav class="text-center text-light" >
  <a class="navbar-item ">
    งานเทคโนโลยีสารสนเทศคณะแพทยศาตร์
    <br>
    __________________________________________
    <br>
    มหาวิทยาลัยเชียงใหม่
  </a>
</nav>

  <!-- Links -->
  <ul class="navbar-nav ml-auto"> <!-- ml-auto ขยับออกจากฝั่งซ้ายเป็นสัดส่วนอัตโนมัติ -->
   <!--  <li class="nav-item">
      <a class="nav-link" href="#">Services</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Blog</a>
    </li> -->

    <li class="nav-item dropdown">

   <!-- <i class="fa fa-th" style="font-size:24px;color:white"></i> -->

   <a class="nav-link dropdown-toggle" href="#" id="navbardrop1" data-toggle="dropdown">
        <img src="pic/grid-icon-63640.png" width="40" height="40" alt="">
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="admin.php"><img src="pic/house.png" width="35" height="35" alt=""> Home</a>
        <a class="dropdown-item" href="#"><img src="pic/approved-512.png" width="35" height="35" alt=""> Approved</a>
        <a class="dropdown-item" href="#"><img src="pic/Programmer-512.png" width="35" height="35" alt=""> Profile</a>
        <a class="dropdown-item" href="contact_admin.php"><img src="pic/Circle-icons-contacts.svg.png" width="35" height="35" alt=""> Contact</a>
       <a class="dropdown-item" href="logout.php"><img src="pic/276363.png" width="35" height="35" alt=""> Logout</a>
      </div>
    
    </li>

    <li class="nav-item dropdown">
      <!-- <?= $email; ?> -->
        <img src="pic/Programmer-512.png" width="50" height="50" alt="">
    </li>
  </ul>
</nav>

		
			<div class="col-lg-4 offset-lg-4 bg-light rounded " >
				
					<div style="margin-top: 6rem; margin-bottom:6rem;">
				<h2 class="text-center mt-2">Contact</h2>
				<form  action="" method="post" role="form" class="p-4" id="login-form">
					
					<div class="form-group">
						<p class=text-left><img src="pic/Old-Mobile-icon.png" width="50" height="50" align="left"alt="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tel : 0988866541 ต่อ 54 <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 66956996</p>
					</div>
					<div class="form-group">
						<p class="text-left"><img src="pic/facebook_circle-512.png" width="50" height="50" alt="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Facebook : ITS Med CMU</p>
					</div>
					
				</form>
			
			</div>
			</div>

    
 
 

	<script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>





</body>
</html>
