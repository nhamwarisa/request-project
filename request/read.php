<?php
//check ว่ามี id มั้ย?

	if(isset($_GET["id"]) && !empty($_GET["id"])){
		require_once'config.php';

		//prepare , select stmt
		
		/*$sql = "SELECT *
		FROM request 
			
		
		WHERE request_id = ?";*/

          $sql ="SELECT *,request_detail_description FROM request as a 
		  INNER join request_detail as b 
		  on a.request_id = b.request_id 
		  WHERE a.request_id = ?";
		





		if ($stmt = mysqli_prepare($link,$sql))
		 {
			mysqli_stmt_bind_param($stmt,"s",$param_request_id);
			$param_request_id = trim($_GET["id"]);
			
			
			
			//ให้ prepare stmt ทำงาน --> check
			if (mysqli_stmt_execute($stmt)) {
				
				$result = mysqli_stmt_get_result($stmt);

				//check resource--> เช็คว่ามีข้อมูลมั้ย มีข้อมูลอยู่ในแถวมั๊ย
				
				if(mysqli_num_rows($result) == 1){
					

					
					$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
					//ดึงค่าจาก ฟิลด์มาเก็บในตัวแปร
					/*
					$request_id = $row["request_id"];
					$request_name = $row["request_name"];
					$request_date = $row["request_date"];
					$head_status = $row["head_status"];
					$head_name = $row["head_name"];
				    $request_des = $row["request_detail_description"];
					*/
				}
				else {

					echo "here";
					// url ไม่ได้เก็บ id ที่ถูกต้อง
					//header("location: index.php");
					//exit();
				}

			} else 
			{
				echo "Oops! something went wrong!";
			}
		}
		mysqli_stmt_close($stmt);

		mysqli_close($link);
	} else {
		//header("location: error.php");
		//exit();
	}


?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie-edge">
	<title>Read Page</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
	<style>
    	.wrapper{
    		width:650px;
    		margin: 0 auto;
    	}
    	.page-header h2{
    		margin top:0;
    	}
    	table tr td:last-child a {
    		margin-right: 15px; 
    	}
    </style>
</head>
<body>
		<div class="wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="page-header">
							<h1>View Request</h1>
						</div>

						<div class="form-group">
							<label>No.</label>
							<p class="form-control-static"><?php echo $row["request_id"]; ?></p>
						</div>

						<div class="form-group">
							<label>Doc No.</label>
							<p class="form-control-static"><?php echo $row["doc_no"]; ?></p>
						</div>



						<div class="form-group">
							<label>Subject</label>
							<p class="form-control-static"><?php echo $row["request_name"]; ?></p>
						</div>
						<div class="form-group">
							<label>Date</label>
							<p class="form-control-static"><?php echo $row["request_date"]; ?></p>
						</div>
						<div class="form-group">
							<label>Status</label>
							<p class="form-control-static"><?php echo $row["head_status"]; ?></p>
						</div>
						<div class="form-group">
							<label>Head Name</label>
							<p class="form-control-static"><?php echo $row["head_name"]; ?></p>
						</div>
						<div class="form-group">
							<label>Description</label>
							<p class="form-control-static"><?php echo $row["request_detail_description"]; ?></p>
						</div>

						<p><a href="index.php" class="btn btn-primary">Back</a></p>
					</div>
				</div>
			</div>
		</div>
</body>
</html>