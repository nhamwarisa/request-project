-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2019 at 08:18 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meddatabase`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `department_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `department_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `department_name`) VALUES
('001', 'งานบริหารทั่วไป'),
('002', 'งานคลัง'),
('003', 'งานซ่อมบำรุง'),
('004', 'งานเทคโนโลยีสารสนเทศ'),
('005', 'งานบริหารงานวิจัย');

-- --------------------------------------------------------

--
-- Table structure for table `dev`
--

CREATE TABLE `dev` (
  `dev_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dev_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dev`
--

INSERT INTO `dev` (`dev_id`, `dev_name`) VALUES
('001', 'บดินทร์ ศรีเรือน'),
('002', 'โอรส ชมพู'),
('003', 'มานพ สูงส่ง'),
('004', 'แพรวา ขยันดี'),
('005', 'ยอด เลิศเลอ'),
('006', 'ชูใจ กาฝาก');

-- --------------------------------------------------------

--
-- Table structure for table `gyr`
--

CREATE TABLE `gyr` (
  `gyr_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gyr_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gyr`
--

INSERT INTO `gyr` (`gyr_id`, `gyr_name`) VALUES
('001', 'ปกติ'),
('002', 'ด่วน'),
('003', 'ด่วนมาก');

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `request_id` int(11) NOT NULL,
  `doc_no` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `request_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `request_date` date NOT NULL,
  `head_status` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `head_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `request_type` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `request_percent` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `confirmer_point` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `confirmer_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`request_id`, `doc_no`, `request_name`, `request_date`, `head_status`, `head_name`, `user_id`, `request_type`, `request_percent`, `confirmer_point`, `confirmer_name`) VALUES
(1, 'RE0001', 'Testing RE0001', '2019-02-15', 'Normal', 'Marisa', '', '', '', '', ''),
(2, 'RE0002', 'Testing RE0002', '2019-02-18', 'Normal', 'Rittichai', '', '', '', '', ''),
(3, 'RE0003', 'Test ka 0003', '2019-02-21', 'Normal', 'Supream', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `request_detail`
--

CREATE TABLE `request_detail` (
  `request_detail_id` int(20) NOT NULL,
  `request_detail_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gyr_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `request_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `request_detail`
--

INSERT INTO `request_detail` (`request_detail_id`, `request_detail_description`, `gyr_id`, `request_id`) VALUES
(1, 'Testing RE0001', '', '1'),
(2, 'testing RE0002', '', '2'),
(3, 'Testing Med Re0003', '', '3');

-- --------------------------------------------------------

--
-- Table structure for table `request_has_dev`
--

CREATE TABLE `request_has_dev` (
  `request_has_dev` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `request_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dev_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `request_has_dev`
--

INSERT INTO `request_has_dev` (`request_has_dev`, `request_id`, `dev_id`) VALUES
('001', '001', '003'),
('002', '001', '004'),
('003', '001', '002'),
('004', '002', '001'),
('005', '002', '005'),
('006', '003', '004'),
('007', '003', '003'),
('008', '003', '005'),
('009', '003', '002'),
('010', '004', '001'),
('011', '004', '003'),
('012', '005', '004'),
('013', '005', '005'),
('014', '005', '003');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_pass` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `department_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_level` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_email`, `user_pass`, `department_id`, `user_level`, `image`) VALUES
('001', 'มาอุโร มัลพิกิ', 'mauro@gmail.com', '123456789', '005', '1', ''),
('002', 'สบตุ๋ย หวังกระแทกคาง', 'soptui@gmail.com', '999999999', '003', '1', ''),
('003', 'จิตรางคนางค์ อัคนีรุทธ์', 'chitrangkhankang@gmail.com', '2342589767', '001', '1', ''),
('004', 'แจ่มศร ีมณีจันทร์', 'chamsri@gmail.com', '5678943512', '002', '1', ''),
('005', 'รจนา เผือกจันทึก', 'rojjana@gmail.com', '455689766', '001', '1', ''),
('006', 'นพดล นิ่มนวล', 'noppadon@gmail.com', '765367997', '001', '1', ''),
('007', 'ชูจิต ชูใจ', 'chuchit@gmail.com', '556768979', '003', '1', ''),
('008', 'มณีรัตน์ อิ่มอุ่น', 'maneerat@gmail.com', '554368979', '003', '1', ''),
('009', 'เจ็ดสี มณีเจ็ดแสง', 'jedsee@gmail.com', '984123556', '002', '1', ''),
('010', 'นกแก้ว สีใส', 'nokkaew@gmail.com', '457332546', '005', '1', ''),
('011', 'บดินทร์ ศรีเรือน', 'bodin@gmail.com', '8487348', '004', '3', ''),
('012', 'โอรส ชมพู', 'oros@gmail.com', '347575949', '004', '3', ''),
('013', 'มานพ สูงส่ง', 'manop@gmail.com', '346267830', '004', '3', ''),
('014', 'แพรวา ขยันดี', 'praewa@gmail.com', '98930333', '004', '3', ''),
('015', 'ยอด เลิศเลอ', 'yod@gmail.com', '74785980', '004', '3', ''),
('016', 'ชูใจ กาฝาก', 'chujai@gmail.com', '799937893', '004', '3', ''),
('017', 'จิตรพร จอมวงศ์', 'chitphon@gmail.com', '8984345498', '004', '2', ''),
('018', 'ศาสตราจารย์ นายแพทย์ บรรณกิจ โลจนาภิวัฒน์', 'bunnakij@gmail.com', '7468673444', '001', '2', ''),
('019', 'สินีนาถ สันติธีรากุล', 'sininad@gmail.com', '565879898', '005', '2', ''),
('020', 'อภิญญา ธนประสิทธิกุล', 'apinya@gmail.com', '4875839587', '002', '2', ''),
('021', 'รศ.นพ.ณัฐพงศ์ โฆษชุณหนันท์', 'natthaphong@gmail.com', '6565423465', '001', '2', ''),
('022', 'ผศ.นพ.วนรักษ์ วัชระศักดิ์ศิลป์', 'wanarak@gmail.com', '874778238', '003', '2', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `dev`
--
ALTER TABLE `dev`
  ADD PRIMARY KEY (`dev_id`);

--
-- Indexes for table `gyr`
--
ALTER TABLE `gyr`
  ADD PRIMARY KEY (`gyr_id`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `request_detail`
--
ALTER TABLE `request_detail`
  ADD PRIMARY KEY (`request_detail_id`),
  ADD KEY `request_id` (`request_id`);

--
-- Indexes for table `request_has_dev`
--
ALTER TABLE `request_has_dev`
  ADD PRIMARY KEY (`request_has_dev`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `request_detail`
--
ALTER TABLE `request_detail`
  MODIFY `request_detail_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
