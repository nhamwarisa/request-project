<?php
//check ว่ามี id มั้ย?

if(!isset($_GET["id"])){
	header("location: error.php");
}
$id = $_GET['id'];
if($id == ''){
	header("location: error.php");
}

require_once 'config.php';

$sql="SELECT request.*, request_detail.request_detail_description 
FROM request  INNER join request_detail 
on request.request_id = request_detail.request_id 
WHERE request.request_id = :request_id";

try {
   
	$stmt = $dbh->prepare($sql);
	$stmt->bindParam("request_id", $id);

	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$row = $rows[0];
	


} catch(PDOException $e) {
	header("location: error.php");
	exit();
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie-edge">
	<title>Read Page</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
	<style>
    	.wrapper{
    		width:650px;
    		margin: 0 auto;
    	}
    	.page-header h2{
    		margin top:0;
    	}
    	table tr td:last-child a {
    		margin-right: 15px; 
    	}
    </style>
</head>
<body>
		<div class="wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="page-header">
							<h1>View Request</h1>
						</div>

						<div class="form-group">
							<label>No.</label>
							<p class="form-control-static"><?php echo $row["request_id"]; ?></p>
						</div>
						<div class="form-group">
							<label>Subject</label>
							<p class="form-control-static"><?php echo $row["request_name"]; ?></p>
						</div>
						<div class="form-group">
							<label>Date</label>
							<p class="form-control-static"><?php echo $row["request_date"]; ?></p>
						</div>
						<div class="form-group">
							<label>Status</label>
							<p class="form-control-static"><?php echo $row["head_status"]; ?></p>
						</div>
						<div class="form-group">
							<label>Head Name</label>
							<p class="form-control-static"><?php echo $row["head_name"]; ?></p>
						</div>
						<div class="form-group">
							<label>Description</label>
							<p class="form-control-static"><?php echo $row["request_detail_description"]; ?></p>
						</div>

						<p><a href="index.php" class="btn btn-primary">Back</a></p>
					</div>
				</div>
			</div>
		</div>
</body>
</html>