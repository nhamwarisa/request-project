<?php
	require_once 'config.php';

	/*define ตัวแปร ให้มันไม่มีค่าอะไรเลย มันจะเป็นค่าว่าง*/
	$request_id = $doc_no = $request_name = $request_date = $head_status = $head_name = $request_detail_description = "";
	$request_id_err = $doc_no_err = $request_name_err = $request_date_err = $head_status_err = $head_name_err = $request_detail_description_err = "";

	// Processing form data whenform is submitted
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		//Validate request_id
		$input_request_id = trim($_POST['request_id']);
		if(empty($input_request_id)){
			$request_id_err = "Please enter an No.";
		}
		else {
			$Requestid = $input_request_id;
		}

			//Validate doc_no
		$input_doc_no = trim($_POST['doc_no']);
		if(empty($input_doc_no)){
			$doc_no_err = "Please enter doc no";
		}
		else {
			$Docno = $input_doc_no;
		}

		//Validate request_name
		$input_request_name= trim($_POST['request_name']);
		if(empty($input_request_name)){
			$request_name_err = "Please enter an Subject";
		}
		else {
			$Requestname = $input_request_name;
		}

			//Validate request_date
		$input_request_date = trim($_POST['request_date']);
		if(empty($input_request_date)){
			$request_date_err = "Please enter an date";
		}
		else {
			$Requestdate = $input_request_date;
		}

			//Validate head_status
		$input_head_status = trim($_POST['head_status']);
		if(empty($input_request_id)){
			$head_status_err = "Please enter an Status";
		}
		else {
			$Headstatus = $input_head_status;
		}

			//Validate head_name
		$input_head_name = trim($_POST['head_name']);
		if(empty($input_head_name)){
			$head_name_err = "Please enter an Head Name";
		}
		else {
			$Headname = $input_head_name;
		}

			//Validate request_detail_description
		$input_request_detail_description = trim($_POST['request_detail_description']);
		if(empty($input_request_detail_description)){
			$request_detail_description_err = "Please enter an Description";
		}
		else {
			$Requestdes = $input_request_detail_description;
		}


		
	
		// check ว่ามี error ไหม
		if(empty($request_id_err) && empty($doc_no_err) && empty($request_name_err) && empty($request_date_err) && empty($head_status_err) && empty($head_name_err)&& empty($request_detail_description))
		{
			// Prpare an insert statement
			$sql = "INSERT INTO request (request_id,doc_no,request_name,request_date,head_status,head_name) VALUE (?,?,?,?,?,?)";
			

			if($stmt = mysqli_prepare($link,$sql))
			{	

				mysqli_stmt_bind_param($stmt,"isssss",$param_request_id,$param_doc_no,$param_request_name,$param_request_date,$param_head_status,$param_head_name);

				$param_request_id = $Requestid;
				$param_doc_no = $Docno;
				$param_request_name = $Requestname;
				$param_request_date = $Requestdate;
				$param_head_status = $Headstatus;
				$param_head_name = $Headname;
				

				// check ว่า ถ้ามีการเพิ่มข้อมูลลงไป ในตารางเเล้ว จะให้มัน redirect ไปหน้า index
				if(mysqli_stmt_execute($stmt))
				{
					header("location: index.php");
				}
				else
				{
					echo "Something went wrong. ";
				}
			}
			mysqli_stmt_close($stmt);
		}

		if(empty($request_detail_description_err))
		{
			// Prpare an insert statement
			
			$sql2 = "INSERT INTO request_detail (request_detail_description) VALUE (?)";
			

			if($stmt2 = mysqli_prepare($link,$sql2))
			{	

				mysqli_stmt_bind_param($stmt2,"s",$param_request_detail_description);

				$param_request_detail_description = $Requestdes;

				// check ว่า ถ้ามีการเพิ่มข้อมูลลงไป ในตารางเเล้ว จะให้มัน redirect ไปหน้า index
				if(mysqli_stmt_execute($stmt2))
				{
					header("location: index.php");
					
				}
				else
				{
					echo "Something2 went wrong.";
				}
			}
			mysqli_stmt_close($stmt2);
		}
	
		mysqli_close($link);
	}


?>




<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width-device-width,initial-scale-1.0">
	<meta http-equiv="X-UA-Compatible" content="ie-edge">
	<title>Create Request</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">



	<style>
		.wrapper{
			width: 500px;
			margin:0 auto;
		}
	</style>
</head>
<body>
	<div class="wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="page-header">
						<h2>Create Request</h2>
					</div>
					<p>Please fill this form and submit to add request to database</p>

					<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
						<div class="form-group <?php echo (!empty($request_id_err)) ? 'has-error' :''; ?>">
							<label>No.</label>
							<input type="number" name="request_id" class="form-control" value="<?php echo $request_id; ?>">
							<span class="help-block"><?php echo $request_id_err;?></span>
						</div>
					
						<div class="form-group <?php echo (!empty($doc_no_err)) ? 'has-error' :''; ?>">
							<label>Doc No.</label>
							<input type="text" name="doc_no" class="form-control" value="<?php echo $doc_no; ?>">
							<span class="help-block"><?php echo $doc_no_err;?></span>
						</div>

						<div class="form-group  <?php echo (!empty($request_name_err)) ? 'has-error' :''; ?>">
							<label>Subject</label>
							<input type="text" name="request_name" class="form-control" value="<?php echo $request_name; ?>">
							<span class="help-block"><?php echo $request_name_err;?></span>
						</div>

						<div class="form-group <?php echo (!empty($request_date_err)) ? 'has-error' :''; ?>">
							<label>Request Date</label>
							<input type="text" name="request_date" class="form-control" value="<?php echo $request_date; ?>">
							<span class="help-block"><?php echo $request_date_err;?></span>
						</div>

						<div class="form-group <?php echo (!empty($head_status_err)) ? 'has-error' :''; ?>">
							<label>Status</label>
							<input type="text" name="head_status" class="form-control" value="<?php echo $head_status; ?>">
							<span class="help-block"><?php echo $head_status_err;?></span>
						</div>

						<div class="form-group <?php echo (!empty($head_name_err)) ? 'has-error' :''; ?>">
							<label>Head Name</label>
							<input type="text" name="head_name" class="form-control" value="<?php echo $head_name; ?>">
							<span class="help-block"><?php echo $head_name_err;?></span>
						</div>

						<div class="form-group <?php echo (!empty($request_detail_description_err)) ? 'has-error' :''; ?>">
							<label>Description</label>
							<textarea type="text" name="request_detail_description" class="form-control"><?php echo $request_detail_description; ?></textarea> 
							<span class="help-block"><?php echo $request_detail_description_err;?></span>
						</div>
						<input type="submit" class="btn btn-primary" value="Submit">
						<a href="index.php" class="btn btn-default">Cancel</a>
					</form>

				</div>
			</div>
		</div>
	</div>

</body>
</html>