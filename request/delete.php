<?php
	if(isset($_POST["request_id"]) && !empty($_POST["request_id"]))
	{
		require_once'config.php';

		$sql = "DELETE FROM request WHERE request_id = ?";

		if($stmt = mysqli_prepare($link,$sql))
		{
			mysqli_stmt_bind_param($stmt,"i",$param_request_id);

			$param_request_id = trim($_POST["request_id"]);

			if (mysqli_stmt_execute($stmt))
			 {
				header("location: index.php");
				exit();
			} 
			else
			{
				echo "Oops! something went wrong.";
			}
		}

		mysqli_stmt_close($stmt);

		mysqli_close($link);

	}
	else
	{
		if(empty(trim($_GET["id"])))
		{
			header("location : index.php");
			exit();
		}
	}


?>




<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie-edge">
	<title>Delete Page</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
	<style>
    	.wrapper{
    		width:650px;
    		margin: 0 auto;
    	}
    
    </style>
</head>
<body>
		<div class="wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="page-header">
							<h1>Delete</h1>
						</div>
						<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
							<div class="alert alert-danger fade in">
								<input type="hidden" name="request_id" value="<?php echo trim($_GET["id"]); ?>">
								<p>Are you sure want to delete this record?</p>
								<p>
									<input type="submit" value="Yes" class="btn btn-danger">
									<a href="index.php" class="btn btn-default">No
								</p>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
</body>
</html>